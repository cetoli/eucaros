EUCAROS
=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------
- to be defined

`21.03`_
----------------

Added
+++++
- Gonial and Gonior with SG Symbols.

Changed
+++++++
- Moved main to eucaros.control.
- Moved eucaros to src.
- Moved _py, util to src

`21.02`_
----------------

Added
+++++
- Readme.
- Changelog.
- Requirements
- Dockerfile
- Heroku
- Scrap para parques
- Pages - Fauna - Ecology
- Page - Menu de Seleção
- Interface Dradis - Gonial funcionando

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _21.02: https://gitlab.com/cetoli/eucaros/-/releases
.. _21.03: https://gitlab.com/cetoli/eucaros/-/releases

.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE


