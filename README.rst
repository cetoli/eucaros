Eucaros
=========
 |Eucaros|

História de uma civilização e a sua evolução ao longo das eras.

 |docs| |python| |license| |github|


:Author:  Carlo E. T. Oliveira
:Version: 21.02
:Affiliation: Universidade Federal do Rio de Janeiro
:License: GNU General Public License v3 or later (GPLv3+)
:Homepage: `Projeto Eucaros`_
:Changelog: `CHANGELOG <CHANGELOG.rst>`_

Eucaros
-------

História de uma civilização e a sua evolução ao longo das eras. O início brutal que segue por toda a sua evolução. As raças que se derivam e lutam pelo domínio civilizatório. A raça antiga e suas conquistas. o declinio desta raça predada pelos seus brutais antecessores. Os sobreviventes e seus refúgios. A raça distante que vem em  resgate dos antigos. A grande pausa e a vida latente. A retomada do projeto dos antigos. As novas raças que surgem deste propósito. As divergências das novas raças. As raças semisintéticas e sintéticas. A disputa pelo legado dos antigos. Embates e alianças entre as raças.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br

.. _Projeto Eucaros: https://activufrj.nce.ufrj.br/wiki/labase/Eucaros_Malanta

.. |github| image:: https://img.shields.io/badge/release-21.02-blue
   :target: https://gitlab.com/cetoli/eucaros/-/releases


.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE

.. |Eucaros| image:: https://imgur.com/HNrJVZ7.png
   :target: https://cetoli.gitlab.io/eucaros/
   :alt: EUCAROS
   :width: 800px

.. |python| image:: https://img.shields.io/github/languages/top/kwarwp/kwarwp
   :target: https://www.python.org/downloads/release/python-383/

.. |docs| image:: https://img.shields.io/readthedocs/supygirls
   :target: https://supygirls.readthedocs.io/en/latest/index.html

.. |license| image:: https://img.shields.io/github/license/kwarwp/kwarwp
   :target: https://gitlab.com/cetoli/eucaros/-/raw/master/LICENSE
