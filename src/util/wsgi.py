#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Serve dynamic version information.

    This module serves version information for the site.

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Classes neste módulo:
    :py:class:`Scrap` Scraps from Parque Nacional page.

Changelog
---------
.. versionadded::    21.03
        Save scrap into YAML.

.. versionadded::    21.02
        Scrap from parques.

.. seealso::

   Page :ref:`eucaros_introduction`

"""
from bottle import route, run, view, static_file, get
import os
import bottle

py_dir = base_path = os.path.abspath(os.path.dirname(__file__))
css_dir = img_dir = template_path = os.path.join(base_path, '..', "eucaros", "view")
spy_dir = os.path.join(base_path, '..', "_spy")
eos_dir = os.path.join(base_path, '..', "eucaros")
bottle.TEMPLATE_PATH.insert(0, template_path)
ITA = """https://i.imgur.com/AD1wScZ.jpg
https://static.thousandwonders.net/Itatiaia.National.Park.original.17451.jpg
https://sp-images.summitpost.org/508602.JPG?auto=format&fit=max&ixlib=php-2.1.1&q=35&w=1024&s=67689616e1a03a7cc00e0015796284db
http://www.dronestagr.am/wp-content/uploads/2016/02/Itatiaia_01-1200x674.jpg
https://i.pinimg.com/736x/33/89/2f/33892ff192ac8748f7f233725c131ca5.jpg
https://cdn.kimkim.com/files/a/content_articles/featured_photos/a7f23c9b52a18ed6529de5c59a6d67c412becee1/big-2c1240b8da65776e5315d56bf51e1990.jpg
https://s.iha.com/00140309766/Itatiaia-The-mountains-of-the-itatiaia-national-park.jpeg
https://static.thousandwonders.net/Itatiaia.National.Park.original.17449.jpg
https://upload.wikimedia.org/wikipedia/commons/7/74/Prateleiras_-_Parque_Nacional_do_Itatiaia%2C_Rio_de_Janeiro_RJ_01.jpg
https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Erupcao_nas_nuvens.jpg/1920px-Erupcao_nas_nuvens.jpg
https://4.bp.blogspot.com/_sjX51bjYiMA/TRdQ9e05rnI/AAAAAAAAOpc/bU4pEjSu0AY/s1600/PARQUE%2BNACIONAL%2BDE%2BITATIAIA.jpg
https://2.bp.blogspot.com/-SUATIG8aXQo/Tg4nJKZBFHI/AAAAAAAACAg/g2Ac3DUnZf8/s1600/525167-19774-1280.jpg""".split()
pages = dict(
    eco=dict(
        pages=[("/eco", "Parque Nacional de Itatiaia", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Parques",
        categoria="Parques Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    pei=dict(
        pages=[("/fau", "Peixes Nacionais", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Peixes",
        categoria="Peixes Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    mam=dict(
        pages=[("/fau", "Mamíferos Nacionais", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Mamíferos",
        categoria="Mamíferos Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    rep=dict(
        pages=[("/fau", "Répteis Nacionais", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Répteis",
        categoria="Répteis Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    ave=dict(
        pages=[("/fau", "Aves Nacionais", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta- Aves",
        categoria="Aves Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    vir=dict(
        pages=[("/eco", "Virus", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Virus",
        categoria="Virus Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    bac=dict(
        pages=[("/eco", "Bactérias", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Bactérias",
        categoria="Bactérias Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    pro=dict(
        pages=[("/eco", "Protozoários", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Protozoários",
        categoria="Protozoários Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    fun=dict(
        pages=[("/eco", "Fungos", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Fungos",
        categoria="Fungos Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
    anf=dict(
        pages=[("/eco", "Anfíbios Nacionais", imge) for imge in ITA],
        pagetitle="Eucaros - Malanta - Anfíbios",
        categoria="Anfíbios Nacionais",
        legenda="Mantido pelo ICMBIO"
    ),
)


@route('/')
@view('index')
def index():
    return {}


@route('/ecm/<pagesel>')
@view('ecomenu')
def eco(pagesel):
    # pages = [("/eco", "Parque Nacional de Itatiaia", "https://bulma.io/images/placeholders/128x128.png")]*8
    # pages = [("/eco", "Parque Nacional de Itatiaia", imge) for imge in ITA]
    return dict(**pages[pagesel])


@route('/eco')
@view('ecofacts')
def eco():
    return {}


@route('/fau')
@view('faunafacts')
def fauna():
    return {}


@get("/_static/<filepath:re:.*..(png|jpg|svg|gif|ico)>")
def img(filepath):
    return static_file(filepath, root=img_dir)


@get("/css/<filepath:re:.*..(css|ttf|js)>")
def ajs(filepath):
    return static_file(filepath, root=css_dir)


@get("/js/<filepath:re:.*..css>")
def ajs(filepath):
    return static_file(filepath, root=css_dir)


# Static Routes
@get("<:path>/__init__.py")
def init_py():
    print("initview_py/__init__.py")
    return ""


# Static Routes
@get("/eucaros/<filepath:path>")
def view_py(filepath):
    print("view_py", filepath, eos_dir)
    return static_file(filepath, root=eos_dir)


# Static Routes
@get("/_spy/<module_name>/<filepath:re:.*..py>")
def spy(module_name, filepath):
    # print("spy", module_name, filepath)
    print("spy", module_name, filepath, os.path.join(spy_dir, module_name))
    return static_file(filepath, root=os.path.join(spy_dir, module_name))


# Static Routes
# @get("/__code/<module_name:re:[a-z].*>/<filepath:re:.*\.py>")
@get("/__code/<module_name>/<filepath:re:.*..py>")
def core_py(filepath, module_name="julia"):
    print("core_py", module_name, filepath, os.path.join(py_dir, module_name))
    return static_file(filepath, root=os.path.join(py_dir, module_name))


run(host='localhost', port=8080)
