#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Serve dynamic version information.

    This module scraps information from the parque site.

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Classes neste módulo:
    :py:class:`Scrap` Scraps from Parque Nacional page.

Changelog
---------
.. versionadded::    21.03
        Save scrap into YAML.

.. versionadded::    21.02
        Scrap from parques.

.. seealso::

   Page :ref:`eucaros_introduction`

"""
import re
import os
import requests
from bs4 import BeautifulSoup
import lxml.html
import lxml.html.clean

passw = os.environ['EUCAROS_MALANTA']
MONGO_DETAILS = f"mongodb+srv://carlotolla:{passw}@eucaros.pothm.mongodb.net/malanta?retryWrites=true&w=majority"
MONGO_DETAILS += '&ssl=true&ssl_cert_reqs=CERT_NONE'


class Scrap:
    PAGE = 'https://pt.wikipedia.org/wiki/Lista_de_unidades_de_conserva%C3%A7%C3%A3o_da_Mata_Atl%C3%A2ntica'
    PARK = 'https://pt.wikipedia.org/wiki/Parque_Nacional_de_Aparados_da_Serra'
    BANK = {}
    PARQ = {}
    PARS = []
    WIKI = 'https://pt.wikipedia.org/{}'
    ITEM = """Estado
Mesorregião
Microrregião
Localidade mais próxima
Área
Criação
Visitantes
Gestão
Coordenadas
Sítio oficial""".split("\n")
    STOP = "Parque Nacional de @Parque Nacional do @Parque Estadual do " \
           "@Parque Estadual @Parque Nacional ".split("@")

    def __init__(self):
        self.ybank = "banco_parques.yaml"
        from pymongo import MongoClient
        self.bank = "banco_parques.json"
        self.client = MongoClient(MONGO_DETAILS)
        self.cleaner = lxml.html.clean.Cleaner(style=True)
        database = self.client.malanta
        # parques = database.get_collection("parques")
        self.parques = database.parques

    def retrieve(self):
        # parque = [parque_helper(parque_) for parque_ in parques]
        data = [pq for pq in self.parques.find()]
        xc = '_id Nome'
        data = {pq['Nome']: {dat: self.clean(pq[dat]) if isinstance(pq[dat], str) else pq[dat]
                             for dat in pq if dat not in xc}
                for pq in data}
        Scrap.BANK.update(**data)
        return data

    def include(self, data):
        # parque = [parque_helper(parque_) for parque_ in parques]
        self.parques.insert_one(data)

    def save(self):
        from json import dump
        from yaml import dump as ydump
        with open(self.bank, "w") as bank_file:
            dump(self.BANK, bank_file)
        with open(self.ybank, "w") as bank_file:
            ydump(self.BANK, bank_file, allow_unicode=True)

        # [self.include(data)for data in self.BANK.values()]

    def populate(self):
        [self.parque(name, wiki) for count, (name, wiki) in enumerate(self.PARQ.items())
         if count < 80]

    def list(self):
        source = requests.get(self.PAGE).text
        soup = BeautifulSoup(source, 'lxml')
        # print(soup.prettify())
        for element in soup.findAll(href=re.compile("^/wiki/Parque.*")):
            href = re.findall('href="(.*)" t', str(element))[0]
            nome = element.text
            for pq in self.STOP:
                if pq in nome:
                    nome = nome.replace(pq, '')
            # [nome.replace(pq, '') for pq in self.STOP if pq in nome]
            if nome in 'Parque EstadualParque ProvincialParque nacional':
                continue
            self.PARQ[nome] = self.WIKI.format(href)
            self.PARS.append([nome, self.WIKI.format(href)])
            # print(href)

    def clean(self, html_text):
        doc = lxml.html.fromstring(html_text)
        doc = self.cleaner.clean_html(doc)
        return str(doc.text_content()).replace(" ", " ").replace('&00000000000', '').replace('&0000000000', '')
        # return html_text

    def heads(self, soup, head='Fauna e flora'):
        _ = self
        title = soup.findAll(class_="mw-headline")
        names = [t.text for t in title]
        content = ""
        if head in names:
            start = names.index(head)
            hdtag = title[start]
            end = names[start + 1]
            tag = hdtag.next_element
            print(">>>", hdtag, ">>>", tag.string)
            # for _ in range(100):
            while end not in repr(tag):
                # print(">>>", repr(tag))
                # print("@@@@@@@@@@", repr(tag)) if end in repr(tag) else None
                content += self.clean(repr(tag))
                tag = tag.next_element
            # print("====", hdtag.next_sibling)
        # print("====>>>>>>>>>>>>>", content)
        soup = BeautifulSoup(content, 'lxml')
        [print('###', nome.text, nome.previous_element.previous_element) for nome in soup.findAll('i')]

    def parque(self, name, page=None):
        pg = page or self.PARK
        source = requests.get(pg).text
        soup = BeautifulSoup(source, 'lxml')
        # self.heads(soup)
        title = soup.find(class_="firstHeading").text
        nam, tit = self.clean(name), self.clean(title)
        content = {'Nome': nam, 'Título': tit}
        content.update(Fauna=[dict(nome='', espécie='')],
                       Imagem=["https://picsum.photos/seed/picsum/300/200"],
                       Relevo=[dict(loc=0, nome='', tipo='')])
        Scrap.BANK.update({name: content}) if name not in self.BANK else None
        source = soup.findAll(class_="infobox_v2")
        soup = BeautifulSoup(str(source), 'lxml')

        # print(soup.prettify())

        def strip(text):
            return self.clean(text.text.strip())

        for element in soup.findAll(['tr']):
            row = BeautifulSoup(str(element), 'lxml')
            col = row.findAll('td')
            if (len(col) > 1) and col[0].text.strip() not in self.ITEM:
                continue
            # print("----->", [t.text for t in col]) if len(col) > 1 else None
            self.BANK[name].update({strip(col[0]): strip(col[1])}) if len(col) > 1 else None

        # self.BANK.update({name: })


if __name__ == '__main__':
    # Scrap().parque()
    scrap = Scrap()
    scrap.retrieve()
    scrap.list()
    scrap.populate()
    # print(Scrap.ITEM)
    # print(Scrap.PARQ)
    # [print(par) for par in Scrap.PARS]
    scrap.save()
    # print(scrap.retrieve())
    # [print(dat, item) for dat, item in .items()]
    print("BANK\n", Scrap.BANK)
