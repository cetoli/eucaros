    <!-- fonts
        # This file is part of  program Eucaros
        # Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
        # `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
        # SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception
    -->
            <aside class="menu is-hidden-mobile">
                <p class="menu-label is-size-4">
                    <span class="is-size-5">Fauna</span>
                </p>
                <ul class="menu-list">
                    <li><a href="/ecm/pei"><i class="fas fa-3x fa-fish"></i></a></li>
                    <li><a href="/ecm/ave"><i class="fas fa-3x fa-crow"></i></a></li>
                    <li><a href="/ecm/mam"><i class="fas fa-3x fa-dog"></i></a></li>
                    <li><a href="/ecm/anf"><i class="fas fa-3x fa-frog"></i></a></li>
                    <li><a href="/ecm/rep"><i class="fas fa-3x fa-wave-square"></i></a></li>
                </ul>
                <p class="menu-label is-size-5">
                    <span class="is-size-5">Parques</span>
                </p>
                <ul class="menu-list">
                    <li><a href="/ecm/eco"><i class="far fa-image fa-3x"></i></a></li>
                </ul>
                <p class="menu-label is-size-3">
                    <span class="is-size-5">Micro</span>
                </p>
                <ul class="menu-list">
                    <li><a href="/ecm/vir"><i class="fas fa-virus fa-3x"></i></a></li>
                    <li><a href="/ecm/bac"><i class="fas fa-bacterium fa-3x"></i></a></li>
                    <li><a href="/ecm/pro"><i class="fas fa-thermometer fa-3x"></i></a></li>
                    <li><a href="/ecm/fun"><i class="fas fa-biohazard fa-3x"></i></a></li>
                </ul>
            </aside>
