<!DOCTYPE html>
<html lang="en">
<head>
    <title>Eucaros - MALANTA - Parques</title>
    <!-- stylesheets -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
    <link rel="shortcut icon" href="epydemic/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.9.0/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/admin.css">
    <script src="https://kit.fontawesome.com/ee888cdf8b.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/brython-dev/brython/3.8.0/www/src/brython.js">
    </script>
</head>
<body onload="brython()" bgcolor="#003333">
<script type="text/python">
        # This file is part of  program Eucaros
        # Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
        # `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
        # SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

        from browser import document, alert, html, window, ajax, timer, svg
        from eucaros.main import main
        from eucaros import __version__


        class Browser:
            document, alert, html, window, ajax, timer, svg = document, alert, html, window, ajax, timer, svg

        #document["_version_"].html = __version__

        #main(Browser)


</script>
<div class="container">
    <div class="columns">
        <div class="column is-1 ">
        <!-- navigation -->
            % include('sidemenu.tpl')
        </div>
        <div class="column is-11">
            <section class="hero is-info welcome is-small">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            Parque Nacional do Itatiaia
                        </h1>
                        <h2 class="subtitle">
                            Serra da Mantiqueira, entre os estados do Rio de Janeiro e Minas Gerais.
                        </h2>
                    </div>
                </div>
            </section>
            <div class="columns is-multiline">
                <div class="column">
                    <div class="box notification is-primary">
                        <div class="heading">Localização</div>
                        <div class="title">Brasil</div>
                        <div class="level">
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Estados</div>
                                    <div class="title is-5">RJ - MG</div>
                                </div>
                            </div>
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Mesorregiões</div>
                                    <div class="title is-5">S:RJ - S,SE:MG</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box notification is-warning">
                        <div class="heading">Dados</div>
                        <div class="title">Area</div>
                        <div class="level">
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Hectares</div>
                                    <div class="title is-5">28 084,10</div>
                                </div>
                            </div>
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Km²</div>
                                    <div class="title is-5">280,8</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box notification is-danger">
                        <div class="heading">posição</div>
                        <div class="title">Coordenadas</div>
                        <div class="level">
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Sul</div>
                                    <div class="title is-5">22° 22' 31"</div>
                                </div>
                            </div>
                            <div class="level-item">
                                <div class="">
                                    <div class="heading">Oeste</div>
                                    <div class="title is-5">44° 39' 44"</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column is-6">
                    <div class="card events-card">
                        <header class="card-header">
                            <p class="card-header-title">
                                Geografia
                            </p>
                            <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                            </a>
                        </header>
                        <div class="card-table">
                            <div class="content">
                                Situa-se geograficamente entre os paralelos 22º19’ e 22º45’ latitude sul e os meridianos
                                44º15’ e 44º50’ de longitude oeste. O parque está localizado no maciço do Itatiaia, na
                                serra da Mantiqueira, no sul dos estados do Rio de Janeiro e de Minas Gerais, com
                                território abrangendo os municípios de Alagoa, Bocaina de Minas, Itamonte, Itatiaia e
                                Resende. O território do parque é cortado pela BR-485. Esta, por cruzar por regioes de
                                até 2 350 m de altitude, é considerada a estrada mais alta do Brasil.[9] O parque se
                                divide em dois ambientes distintos:

                                Sede do Parque (Parte baixa): Saindo do Rio de Janeiro ou São Paulo, segue-se pela
                                Rodovia Presidente Dutra (BR 116) até a cidade de Itatiaia, altura do km 316. O Centro
                                de Visitantes, localizado na parte baixa do parque, possui um museu com informações
                                básicas sobre a fauna e a flora da região, com animais empalhados e uma biblioteca.
                                Planalto (Parte alta): Saindo do Rio de Janeiro ou São Paulo, segue-se pela Rodovia
                                Presidente Dutra (BR-116) até Engenheiro Passos, altura do quilômetro 330, seguindo pela
                                rodovia BR-354 em direção a Itamonte.[12]
                            </div>
                        </div>
                        <footer class="card-footer">
                            <a href="#" class="card-footer-item">View All</a>
                        </footer>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="card events-card">
                        <header class="card-header">
                            <p class="card-header-title">
                                Fauna
                            </p>
                            <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                            </a>
                        </header>
                        <div class="card-table">
                            <div class="content">
                                Na encosta voltada para o Vale do Paraíba, predomina a mata Atlântica com fauna e flora
                                ricas e exuberantes, herbácea e possui o maior índice de endemismos, ou seja, é composta
                                por espécies que só ocorrem ali, como bromélias e orquídeas entre outras. É uma das
                                quatro únicas localidades onde pode ser encontrada uma árvore ameaçada de extinção, a
                                Buchenavia hoehneana.[13]

                                A fauna da parte baixa é mais rica, propicia mais abrigo para mamíferos, como a paca, o
                                quati e algumas espécies de maior porte, como a onça-parda, porcos-do-mato e queixadas.
                                Com grande diversidade de pássaros, como o beija-flor (colibri, beija-flor-de-cor-roxa
                                entre outros), assim como tucanos-de-bico-verde e guachos. A importância do Itatiaia
                                para a conservação de espécies de aves é grande tendo em vista os frugívoros de grande
                                porte e as espécies habitantes das partes altas.
                            </div>
                        </div>
                        <footer class="card-footer">
                            <a href="#" class="card-footer-item">View All</a>
                        </footer>
                    </div>
                </div>
            </div>
            <nav class="panel is-info">
                <p class="panel-heading">
                    Galeria de Imagens
                </p>
                <div class="panel-block">
                    <p class="control">&nbsp;</p>
                </div>
                <div class="columns is-multiline">
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns is-multiline">
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="https://bulma.io/images/placeholders/1280x960.png"
                                         alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">

                                <div class="content">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                    <a href="#">#css</a> <a href="#">#responsive</a>
                                    <br>
                                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
<script async type="text/javascript" src="/js/bulma.js"></script>
</body>
</html>