<!DOCTYPE html>
<html lang="en">
<head>
    <title>Eucaros - MALANTA</title>
    <!-- stylesheets -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ee888cdf8b.js" crossorigin="anonymous"></script>
    <link rel="shortcut icon" href="/eucaros/view/favicon.ico" type="image/x-icon"/>
    <script type="text/javascript" src="https://cdn.rawgit.com/brython-dev/brython/3.8.0/www/src/brython.js">
    </script>
    <style>
    @font-face {
    font-family: anquietas;
    src: url('/css/anquietas.ttf');
    }
    @font-face {
    font-family: gonia;
    src: url('/css/gonia.ttf');
    }
    @font-face {
    font-family: gonior;
    src: url('/css/gonior.ttf');
    }
    </style>
</head>
<body onload="brython()" bgcolor="#003333">
<script type="text/python">
        # This file is part of  program Eucaros
        # Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
        # `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
        # SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

        from browser import document, alert, html, window, ajax, timer, svg
        from eucaros.control.main import main
        from eucaros.__version__ import __version__


        class Browser:
            document, alert, html, window, ajax, timer, svg = document, alert, html, window, ajax, timer, svg

        document["_version_"].html = __version__

        main(Browser)






</script>
<div id="_main_" style="position:relative; width:100%; height:100%;">
    <div id="pydiv" style="margin: 0 auto; position:relative; top:30px;">
        <img src="https://i.imgur.com/cYr8Lso.jpg" alt="EUCAROS"
             style="width:100vw; height:100vh; object-fit: cover;"/>
        <div style="position: absolute; top: 50%; left: 50%; opacity:0.3;
            margin-top: -100px; margin-left: -100px; width: 200px; height: 200px;">
            <img src="https://imgur.com/uc56UAq.gif" width="200px" alt="LOADING" style=""/>
        </div>
    </div>
    <div id="ctldiv" style="float: left; margin: 0 auto; position:relative; top:30px; left:-200px;">
        <!-- Sensons and atuators -->
        % include('ctlsens.svg')

    </div>
    <div id="_city_" style="float: left;"></div>
</div>
<div style="position:absolute; top:700px; left:2px; height: 10px;">
    <p>
            <span id="_version_" style="color:#FFFFEE;font-size:8px;font-family:courier new,courier,monospace;">
                loading..</span>
            <span id="_lantean_" style="color:#FFFFEE;font-size:8px;font-family:anquietas;">
                eucaros malanta</span>
    </p>
</div>

</body>
</html>