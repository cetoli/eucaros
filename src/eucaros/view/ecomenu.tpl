<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{pagetitle}}</title>
    <!-- stylesheets -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
    <link rel="shortcut icon" href="epydemic/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.9.0/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/admin.css">
    <script src="https://kit.fontawesome.com/ee888cdf8b.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/brython-dev/brython/3.8.0/www/src/brython.js">
    </script>
    <style>
        .box {
        background-color: #20B2AA;
        }
    </style>
</head>
<body onload="brython()" bgcolor="#003333">
<script type="text/python">
        # This file is part of  program Eucaros
        # Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
        # `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
        # SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

        from browser import document, alert, html, window, ajax, timer, svg
        from eucaros.main import main
        from eucaros import __version__


        class Browser:
            document, alert, html, window, ajax, timer, svg = document, alert, html, window, ajax, timer, svg

        #document["_version_"].html = __version__

        #main(Browser)








</script>
<div class="container">
    <div class="columns">
        <div class="column is-1 ">
            <!-- navigation -->
            % include('sidemenu.tpl')
        </div>
        <div class="column is-11">
            <section class="hero is-info welcome is-small">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            {{categoria}}
                        </h1>
                        <h2 class="subtitle">
                            {{legenda}}.
                        </h2>
                    </div>
                </div>
            </section>
            <div class="columns is-multiline is-centered has-text-centered mt-5">
                % for count, (pagelink, page, image) in enumerate(pages):

                <!-- start of post -->
                <div class="column is-3">
                    <div class="box">
                        <!-- image for post -->
                        <div class="card is-3by2" style="height:114px; overflow:hidden;">
                            <figure>
                                <a href="{{pagelink}}">
                                    <img src="{{image}}" width="256px" alt="Image"
                                         style="position:relative; min-width:256px;"></a>
                            </figure>
                        </div>
                        <!-- end of image for post -->

                        <!-- post header -->
                        <div class="card-content-header">
                            <a class="subtitle is-6" href="{{pagelink}}">{{page}}</a>
                        </div>
                    </div>
                </div>
                <!-- end of post -->
                % end
            </div>
        </div>
    </div>
</div>
<script async type="text/javascript" src="/js/bulma.js"></script>
</body>
</html>