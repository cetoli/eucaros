#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Long range sensor for the game I.M.A.D.I.S..

    Interface Multifuncional de Aquisição de Dados e Inteligência Sensorial..

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Classes neste módulo:

    :py:class:`Somatron` Remote object registry.

    :py:class:`Imadis` Long range sensor controller.

Changelog
---------
.. versionadded::    21.03
        Icon catalogue.

.. versionadded::    21.03.a
        Geometric Gonior. Clavus e Zoonus.

.. seealso::

   Page :ref:`eucaros_introduction`

"""
import string
from collections import namedtuple
from math import pi, asin, sin, cos, sqrt, degrees
from random import randint, choice


class Somatron:
    ANIMAL = 'eucaros/view/animaisss-min.png'
    CLAVI = "smith intel market mage_sw " \
            "trickster enemy mentor guide".split()
    ZOONI = "mamal bird amphibian reptile fish".split()
    PROVI = "grain vege cache cure fungi plant energy".split()
    LOCI = "bless base crypt temple rune clash scroll".split()
    SETRI = []
    SETIM = """https://imgur.com/q308L34
https://imgur.com/v1CVsjO
https://imgur.com/7cXtyqA
https://imgur.com/KZoXpRI
https://imgur.com/WyiiE0T
https://imgur.com/jHnybc0
https://imgur.com/iILubFM
https://imgur.com/regZ51L
https://imgur.com/fUDoUc0
https://imgur.com/CljPeMU
https://imgur.com/bDqM8oP
https://imgur.com/SghQp9q
https://imgur.com/o8Hj2F4
https://imgur.com/ctDxOKA
https://imgur.com/gWvkAiR
https://imgur.com/SJ428kZ
https://imgur.com/kQWpngJ""".splitlines()

    class Clavum:
        def __init__(self, name, pos):
            self.name, self.pos = name, pos
            self.pos_size = self.icon = self.setrum = self.sprite = None
            self.choose_sprite()

        @staticmethod
        def create(name, pos):
            return Somatron.SOMI[name](name, pos)

        def choose_sprite(self):
            self.icon = off = Imadis.ICON.index(self.name)
            self.pos_size = (off % 7, off // 7), (7, 6), (256, 256), 0
            # sprite = Somatron.ZOONI.index(self.name) if self.name in Somatron.ZOONI else False
            self.sprite = Imadis.ICOIM

        def enter(self, setrum, pos=None):
            self.pos = pos or self.pos
            self.setrum = setrum
            return self

        def vais(self, pos):
            dx, dy = pos
            x, y = pos
            return self.icon, (x - dx, y - dy)

        def vai(self, pos):
            dx, dy = pos
            x, y = pos
            return self.icon, (x - dx, y - dy)

        def beep(self):
            return self.name, self.pos

        def update_sprite(self, w):
            _ = self
            # off = Imadis.ICON.index(self.name)
            (i, j), (k, l), (m, n), h = self.pos_size

            return self.sprite, (i * -w, j * -w),  (w * k, w * l), 1, m, n, h
            # return Imadis.ICOIM, (off % 7 * -w, off // 7 * -w),  (w * 7, w * 6), 1, 256, 256, -20

    class Clavus(Clavum):

        def choose_sprite(self):
            self.icon = off = randint(0, 2)
            hue = Somatron.CLAVI.index(self.name)*24
            self.pos_size = (off % 3, off // 3), (3, 1), (250, 600), hue
            # sprite = Somatron.ZOONI.index(self.name) if self.name in Somatron.ZOONI else False
            self.sprite = Imadis.SILCOM

    class Zoonus(Clavum):

        def choose_sprite(self):
            self.icon = off = randint(0, 2) + Somatron.ZOONI.index(self.name)*3
            self.pos_size = (off % 3, off // 3), (3, 4), (256, 256), 0
            # sprite = Somatron.ZOONI.index(self.name) if self.name in Somatron.ZOONI else False
            self.sprite = Imadis.ZOONOM

        def _update_sprite(self, w):
            _ = self
            off = Imadis.ICON.index(self.name)

            return Imadis.ICOIM, (off % 7 * -w, off // 7 * -w),  (w * 7, w * 6), 1, 256, 256, -20

    class Setrum:
        def __init__(self, name, pos):
            self.name, self.pos = name, pos
            self.somi = []
            self.cena = Imadis.C()
            self.ir = self._vai

        def vai(self):
            self.ir()
            # print("Setrum vai", self.cena, self.ir, Somatron.SETIM[:2])
            return self.cena

        @staticmethod
        def build():
            Somatron.SETRI = [[Somatron.Setrum("abcd", (x, y)) for x in range(36)]
                              for y in range(36)]

        def somator(self):
            return [s for s in self.somi]

        def enter(self, soma):
            soma.enter(self.cena)
            self.somi.append(soma)

        def leave(self, soma):
            self.somi.remove(soma)

        def _vai(self, _=0):
            self.cena = Imadis.C(choice(Somatron.SETIM) + ".png")

            self.cena.vai()
            Imadis.V.elt.remove()
            self.ir = self.vai_
            return self

        def vai_(self, _=0):
            self.cena.vai()
            Imadis.V.elt.remove()
            return self

    def __init__(self):
        def enter_setrum(soma, pos):
            x, y = pos
            somum = Somatron.Clavum.create(soma, pos)
            setri_size = 36 * 2
            _ = Somatron.SETRI[x // setri_size][y // setri_size].enter(somum)
            Imadis.SOMI.append(somum)
            print(soma, pos, end="|")
            return somum.vai((0, 0))

        clavi, loci, zooni, provi = 8, 6, 12, 10
        print("Somatron", clavi, loci, zooni, provi)
        self.Setrum.build()
        sx, sy = self.setrumxy = (18, 18)
        self.setrum = self.SETRI[sy][sx]
        somi = ((self.CLAVI, clavi), (self.LOCI, loci), (self.ZOONI, zooni), (self.PROVI, provi))
        sx = sy = 2 * 1200
        somi_loci = [(somum, (randint(0, sx), randint(0, sy)))
                     for somum, somi_count in somi
                     for _ in range(2 * somi_count)
                     ]
        print("Somatron", somi_loci)
        self.somi = [enter_setrum(choice(somum), pos) for somum, pos in somi_loci if somum]

    def change_setrum(self, silcon):
        print("change_setrum", silcon, silcon[0]//72 % 36)
        setrumx, setrumy = [sord//72 % 36 for sord in silcon[:2]]
        print("change_setrum", setrumx, setrumy, silcon, silcon[0]//72 % 36)
        if self.setrumxy != (setrumx, setrumy):
            self.setrumxy = (setrumx, setrumy)
            self.setrum = self.SETRI[setrumy][setrumx]
            return True
        return False

    def vai(self):
        return self.setrum.vai()

    @staticmethod
    def base36encode(number):
        # if not isinstance(number, int):
        #     raise TypeError('number must be an integer')
        if number < 0:
            raise ValueError('number must be positive')

        alphabet, base36 = ['0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', '']

        while number:
            number, i = divmod(number, 36)
            base36 = alphabet[i] + base36
        return base36 or alphabet[0]

    @staticmethod
    def base36decode(number):
        return int(number, 36)

    SOMI = {clavi: Clavum for clavi in PROVI+LOCI}
    SOMI.update({clavi: Clavus for clavi in CLAVI})
    SOMI.update({zoonus: Zoonus for zoonus in ZOONI})


class Imadis:
    """ I.M.A.D.I.S Interface Multifuncional para Aquisição de Dados e Inteligência Sensorial

    Classes neste módulo:

        :py:class:`Gonial` Current Address and Forward Trust.

        :py:class:`Gonior` Current Headind and Attitude Trust.

        :py:class:`Beep` Remote object marker.

    """
    BY = None
    ICOIM = "eucaros/view/imadis_icons.png"
    SILCOM = "eucaros/view/silcon.png"
    ZOONOM = "eucaros/view/animaisss-min.png"
    ICON = "xp fight smith intel grain shield market " \
           "look base vege close mage_sw crypt temple " \
           "feed see mamal bird fish bless target_p " \
           "trickster cache cure fungi enemy proto plant " \
           "crow energy rune run reptile sage clash " \
           "targeted scroll amphibian virus range mentor guide".split()

    ICONT = namedtuple("Icon", ICON)
    GUI = object()
    SETRUMSIZE = 36
    A = None
    C = None
    V = None
    ISIZE = (64 * 7, 64 * 6)
    GONIAL = string.ascii_lowercase + string.ascii_uppercase[:10]
    GONIALS = ''.join(letter for letter in GONIAL)
    SOMI = []
    BEEP = {}
    BASE36 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    GONIALTRAN = str.maketrans(BASE36, GONIALS)

    class Gonial:
        def __init__(self, location, cena, pos=(1, -28), size=48):
            self.location, self.pos = location, pos
            x, y = pos
            w = h = size
            letters = self.letters = Imadis.GONIAL
            gonial = ''.join(choice(letters) for _ in range(8))
            print("Gonial", location, pos, cena, Imadis.A)
            self.elt = Imadis.A('', x=x, y=y, w=w * 8, h=h, cena=cena)
            style = dict(color='#FFFFEE', fontSize='20px', fontFamily='gonia')
            self.gonials = Imadis.BY.html.SPAN(gonial, id="_gonial_", style=style)
            _ = self.elt.elt <= self.gonials
            self.elt = Imadis.A('', x=x, y=y, w=w * 8, h=h, cena=cena)
            self.elt.style.opacity = 0
            self.display(location)

        def display(self, intgonials):
            im = Imadis.GONIALTRAN
            gonials = [loc+silco for loc, silco in zip(self.location, intgonials)]
            print("display", intgonials, Somatron.base36encode(intgonials[0]), gonials)
            gx, gy = [Somatron.base36encode(coord//2).translate(im) for coord in gonials[:2]]
            star_gonials = ''.join(f'{sx}{sy}' for sx, sy in zip(gx, gy))[-8:]
            self.gonials.html = star_gonials
            print("display html", star_gonials, Somatron.base36encode(intgonials[0]).translate(im))

    class Gonior:
        def __init__(self, location, cena, pos=(380, -32), size=36):
            self.location, self.pos = location, pos
            x, y = pos
            # w = h = size
            import string
            self.letters = string.ascii_uppercase[2:] + string.ascii_lowercase[:14]
            self.letters += self.letters
            # gonior = randint(0, 40)
            self.gonior = 0
            gonia = self.letters[self.gonior: self.gonior + 18]
            gonial = ["{} ".format(di) for di in gonia]
            print("Gonior", location, pos, cena, gonia, gonial)
            self.elt = Imadis.A('', x=x, y=y, w=710, h=size, cena=cena)
            style = dict(color='#FFEE66', fontSize='26px', fontFamily='gonior', userSelect=None)
            self.gonidial = Imadis.BY.html.SPAN(gonial, id="_gonior_", style=style)
            _ = self.elt.elt <= self.gonidial
            self.elt = Imadis.A('eucaros/view/gonior.png', x=365, y=y, w=720, h=size, cena=cena)
            self.elt.style.opacity = 1

        def azimuth(self, increment):
            self.gonior = (self.gonior - increment)
            self.gonior = 0 if self.gonior >= 36 else 35 if self.gonior < 0 else self.gonior
            gonia = self.letters[self.gonior: self.gonior + 18]
            gonial = " ".join(gonia)
            # gonial = ["{} ".format(di) for di in gonia]
            self.gonidial.html = gonial

    class Beep:
        WS = namedtuple("Winds", "ox oy rx ry")
        WIND = [WS(0, 0, -1, -1), WS(1, 1, 0, 0), WS(-1, -1, 0, 0), WS(0, 0, -1, -1)]

        def __init__(self, soma, cena, silcon, size=128):
            _ = size
            self.name, self.pos = soma.beep()
            self.elt = Imadis.A()
            self.update_beep(soma, cena, silcon)
            Imadis.BEEP[soma] = self

        def update_beep(self, soma, cena, silcon, size=128):
            name, pos = self.name, self.pos = soma.beep()
            ssz = Imadis.SETRUMSIZE
            j, k, t = silcon
            x, y = pos
            delta_clavum = sqrt(x ** 2 + y ** 2) + 0.05
            delta_silcon = sqrt(j ** 2 + k ** 2) + 0.05

            theta_clavum = asin(y / delta_clavum) if x > 0 else asin(y / delta_clavum) + pi
            theta_silcon = t - theta_clavum
            x = int(delta_clavum * sin(theta_silcon))
            y = -int(delta_silcon - delta_clavum * cos(theta_silcon))
            nx, ny = x + 600 if -600 < x < 600 else -10000, 500 - y if 500 > y > 0 else -10000
            dts, dtc = degrees(theta_silcon), degrees(theta_clavum)
            d = sqrt(x ** 2 + y ** 2) + 0.05
            w = min(size, int((256 * size) // d))
            off = Imadis.ICON.index(name)
            pos = (off % 7 * -w, off // 7 * -w)
            icon, opacity, siz = Imadis.ICOIM, 0.75, (w * 7, w * 6)
            hue = 0
            if (-ssz < x < ssz) and (0 < y < ssz):
                nx, ny, w = x * 8 + 500, 470 - y * 2, 256
                icon, pos, siz, opacity, w, h, hue = soma.update_sprite(w)
            self.elt.elt.remove()
            self.elt = Imadis.A(
                icon, tit=f"{y:4.1f} x:{x:4.1f} t{dtc:3.1f} h{hue}", x=nx, y=ny, o=0.9, w=w, h=w, cena=cena)
            self.elt.o = opacity
            self.elt.pos = pos
            self.elt.siz = siz

            self.elt.elt.style.filter = f"hue-rotate({hue}deg)"

        def setrum_presence(self, icon, cena, locate, index, hue=90):
            pos, siz = index
            nx, ny, w, h = locate
            self.elt.elt.remove()
            self.elt = Imadis.A(
                icon, tit=f"l:{locate} ix:{index}", x=nx, y=ny, o=0.9, w=w, h=h, cena=cena)
            self.elt.o = 1
            self.elt.pos = pos
            self.elt.siz = siz
            self.elt.elt.style.filter = f"hue-rotate({hue}deg)"

        def clavum_shield(self, cena, locate, siz):
            name = "guide"
            index = Imadis.ICON.index(name)
            nx, ny, w, h = locate
            pos = index % 7 * -w, index // 7 * -h
            # pos = 7, 6
            self.elt.elt.remove()
            self.elt = Imadis.A(
                Imadis.ICOIM, tit=f"l:{locate} ix:{pos}", x=nx, y=ny, o=0.9, w=w, h=h, cena=cena)
            self.elt.o = 1
            self.elt.pos = pos
            self.elt.siz = siz

        @staticmethod
        def create(soma, cena, silcon=(500, 500)):

            Imadis.BEEP[soma].update_beep(soma, cena, silcon) \
                if soma in Imadis.BEEP else Imadis.Beep(soma, cena, silcon)

    def __init__(self, cena, v, b):
        _ = cena
        Imadis.GUI = v
        Imadis.A, Imadis.C, Imadis.V = v.a, v.c, v.i
        Imadis.BY = b
        self.somtron = Somatron()
        self.cena = self.somtron.setrum.cena
        offx = offy = 36**4 - 2000
        self.location = (int(offx+randint(500, 1000)*36*36), int(offy+randint(500, 1000)*36*36))
        silcon = (int(500+randint(0, 256)), int(500+randint(0, 256)))

        self.silcon = (silcon[0], silcon[1], pi / 2)
        # self.gonior = [v.c(vai=self.left), v.c(vai=self.middle), v.c(vai=self.right)]
        self.gonior = self.gonial = None

    def show_dux(self, dux_item=None):
        sh_siz = 45
        dux_shield = self.SOMI[2]
        shield = Imadis.Beep(dux_shield, self.cena, self.silcon)
        dux_item = dux_item or self.SOMI[0]
        beep = Imadis.Beep(dux_item, self.cena, self.silcon)
        shield.clavum_shield(
            cena=self.cena,
            locate=("50%", "35%", sh_siz, sh_siz), siz=(sh_siz*7, sh_siz*6))
        beep.setrum_presence(
            icon='eucaros/view/silcon.png', cena=self.cena,
            locate=(100, 200, 250, 500), index=((0, 0), (750, 500)))
        _ = beep.elt.elt <= shield.elt.elt

    def move_silco(self, gonial):
        self.silcon = gonial
        self.vai() if self.somtron.change_setrum(gonial[:2]) else None
        self.gonial.display(gonial[:2])

    def right(self, _=0):
        x, y, t = self.silcon
        x += 50
        t -= pi / 2
        # t = t - 2pi
        self.silcon = (x, y, t)
        self.somtor(self.cena, self.silcon)

    def left(self, ev):
        x, y, t = self.silcon
        c = ev.pageX
        d = 495 / 8
        c = (388 - c) // d
        d = int(2 ** c * 5)
        # y -= dy
        x, y = int(d * cos(t) + x), int(d * sin(t) + y)

        print("left", c, x, y, d)
        # self.silcon = (x, y, t)
        self.move_silco(gonial=(x, y, t))
        self.somtor(self.cena, self.silcon)

    def middle_(self, ev):
        c = ev.pageX
        c = (c - 388) / 38.5 + 1
        dx = dy = 0
        x, y, t = self.silcon
        if c <= 5:
            dx = int(6 - c) * 5
            x += dx
        if 5 < c < 15:
            dy = int(abs(10 - c) + 1) * 5
            y -= dy
        if c >= 15:
            dx = int(c - 15) * 5
            x -= dx
        print("mid", c, x, y, dx, dy)
        self.silcon = (x, y, t)
        self.somtor(self.cena, self.silcon)

    def middle(self, ev):
        c = ev.pageX
        # dt = pi / 36
        dt = pi / 2**9
        c = (c - 388) / 38.5
        dx = dy = 0
        x, y, t = self.silcon
        c = min(17, c)
        pr = [9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12]
        # gt = gt + 1 if (c - 388) >= 385 else gt - 1
        gt = pr[c]
        self.gonior.azimuth(gt)
        t += dt * gt**2 * (gt/abs(gt))
        t = t if t < 2 * pi else t - 2 * pi
        print("mid", c, gt, x, y, dx, dy)
        self.silcon = (x, y, t)
        self.somtor(self.cena, self.silcon)

    def vai(self):
        self.cena = cena = self.somtron.vai()
        gl = self.gonial = self.Gonial(self.location, cena=cena)
        gl.elt.vai = self.left
        gr = self.gonior = self.Gonior(0, cena=cena)
        gr.elt.vai = self.middle
        # self.somtor(cena, self.silcon)
        # self.move_silco(self.silcon)
        self.show_dux()
        return self.cena

    def somtor(self, cena, silcon):
        _ = cena, silcon
        # self.Beep(name=name, pos=(bx, by), cena=cena, size=size)
        cena, silcon = self.cena, self.silcon
        # self.SOMI[0].pos = (563, 500 - 1)
        # self.SOMI[1].pos = (500 - 63, 500 - 1)
        # self.SOMI[2].pos = (500, 500 - 63)
        [self.Beep.create(soma, cena, silcon) for soma in self.SOMI]
