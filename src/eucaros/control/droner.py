# lorinda.anastasia.droner.py
# SPDX-License-Identifier: GPL-3.0-or-later
""" Jogo de direcionamento de drones.

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Changelog
---------
.. versionadded::    21.03
        Classe Droner.

"""
from _spy.vitollino.main import Jogo, STYLE
from browser.timer import set_timeout
from collections import namedtuple

"""Usa o timer do navegador para dar um tempinho inicial"""
Rosa = namedtuple("Rosa", "norte leste sul oeste")
STYLE.update(width=1350, height="600px")
# SF = {"transition": "left 8s, top 8s"}
J = Jogo()
NC = J.c()
"""Usa o recurso novo do Vitollino Jogo. Jogo.c é Cena, Jogo.a é Elemento, Jogo.n é Texto"""
SF = {"font-size": "12px", "color": "red", "transition": "left 5s, top 5s"}
"""Dá o tamanho da letra da legenda e faz a legenda se movimentar suavemente quando inicia e acerta"""
VAZIO = "https://i.imgur.com/npb9Oej.png"
BORDA = "https://i.imgur.com/npb9Oej.png"
KNOB = "https://i.imgur.com/v8Lqqpt.png"
ROSA = Rosa((0, -1), (1, 0), (0, 1), (-1, 0))
CIS = {ROSA.norte: ROSA.leste, ROSA.leste: ROSA.norte, ROSA.sul: ROSA.oeste, ROSA.oeste: ROSA.sul}
TRS = {ROSA.norte: ROSA.oeste, ROSA.leste: ROSA.sul, ROSA.sul: ROSA.leste, ROSA.oeste: ROSA.norte}
SWP = {0: CIS, 90: TRS}
GAP = 60


class A:
    def __init__(self, *args):
        pass


class Droner:
    """ Jogo que direciona drones para atingir alvos
    """
    CENA = "https://i.imgur.com/AD1wScZ.jpg"
    CELULA = "https://i.imgur.com/tcCj6nw.png"
    DRONE = "https://i.imgur.com/ZfrJcyE.jpg"  # https://i.imgur.com/XDuFNZw.png"
    KNOBS = 60

    def __init__(self, cena):
        class Anteparo(J.a):
            """ Um bloqueio que desvia o drone para esquerda ou direita

            Qualquer anteparo clicado gira todos os anteparos, mudando de esqurda para direita e vice-versa.

            :param    x: a posição horizontal do anteparo
            :param    y: a posição vertical do anteparo
            :param jogo: o jogo que este anteparo aparece
            :param cena_: a cena onde o anteparo aparece
            :param img: imagem de fundo do anteparo
            """

            def __init__(self, x, y, cena_, jogo, img=KNOB):
                pw = ph = Droner.KNOBS * 2
                self.jogo = jogo
                self.index = x + y * 11
                x, y = GAP + 2 * GAP * x, int(-0.5 * GAP) + 2 * GAP * y

                super().__init__(img, x=x, y=y, w=pw, h=ph, cena=cena_)
                self.elt.onclick = self.rodar
                # self.elt.html = f"{x};{y}"
                self.rotate = self.jogo.rotate

            def cheguei(self, drone, azimuth):
                """O drone chega ao anteparo e precisa ser direcionado"""
                dx, dy = azimuth = SWP[self.rotate][azimuth]
                destino, x, y = self.jogo.localiza(self.index, dx, dy)
                self.elt.html = f"{azimuth} {x}:{y} {str(destino)}"
                # drone.segue(destino, azimuth, x, y, 1)

            def rodar(self, _=None):
                """Quando o jogador clica, gira os anteparos"""
                self.jogo.rotate = self.rotate = (self.rotate + 90) % 180
                self.jogo.rodar(self.rotate)
                self.elt.style.transform = f"rotate({self.rotate}deg)"

            def partida(self):
                """retorna um anteparo normal"""
                return self

            def localiza(self):
                """retorna a posição do anteparo corrente e o azimuth indicado para drone"""
                return self, self.x, self.y

            def roda(self, rodado=0):
                """Gira o anteparo para a rotação dada"""
                self.rotate = rodado
                self.elt.style.transform = f"rotate({self.rotate}deg)"

            def __repr__(self):
                return f"i:{self.index}\nx:{self.x}\ny:{self.y}\n"

            def __str__(self):
                return f"i:{self.index} x:{self.x} y:{self.y}"

        class Borda(Anteparo):
            """ Um bloqueio que para o drone e o relocaliza para uma outra borda aleatória.

            Qualquer anteparo clicado gira todos os anteparos, mudando de esqurda para direita e vice-versa.

            :param    x: a posição horizontal do anteparo
            :param    y: a posição vertical do anteparo
            :param jogo: o jogo que este anteparo aparece
            :param cena_: a cena onde o anteparo aparece
            :param img: imagem de fundo do anteparo
            """

            def __str__(self):
                return f"b:{self.index} x:{self.x} y:{self.y}"

            def __init__(self, x, y, cena_, jogo, img=Droner.DRONE):
                super().__init__(x=x, y=y, jogo=jogo, cena_=cena_, img=img)
                self.o = 0.5
                borda = self

                class Partida(Anteparo):

                    def cheguei(self, drone, azimuth):
                        """O drone chega ao anteparo e precisa ser direcionado"""
                        dx, dy = self.azimuth
                        destino, gx, gy = self.jogo.localiza(borda.index, dx, dy)
                        drone.segue(destino, self.azimuth, gx, gy, 1)
                        drone.elt.html = f"{self.azimuth} $ {gx} {gy}"

                self._partida = Partida(x, y, NC, jogo)
                ax = 0 if (0 < x < 10) else (1 if x == 0 else -1)
                ay = 0 if (0 < y < 5) else (1 if y == 0 else -1)
                self._partida.azimuth = ax, ay
                self.elt.html = f"{ax}<->{ay}"

            def partida(self):
                """retorna um anteparo normal"""
                return self._partida

            def rodar(self, ev=None, nome=None):
                pass

            def roda(self, rodado=0):
                pass

            def cheguei(self, drone, azimuth):
                """O drone chega ao anteparo e precisa ser direcionado"""
                from random import choice, randint
                x, y = randint(1, 9), randint(1, 4)
                ax, ay = azimuth = choice(list(ROSA))  # (0, 1)-1, 0  #
                cx, cy = [10, x, 0][ax + 1], [5, y, 0][ay + 1]
                # cy = 0
                index = cx + cy * 11
                destino, x, y = self.jogo.localiza(index)
                # cx, cy = GAP + 2 * GAP * cx, int(-0.5 * GAP) + 2 * GAP * cy

                # print("localiza", cx, cy, azimuth)
                # self.jogo.start()
                # self.jogo.drone.seguir()
                # return index, cx, cy, azimuth
                self.elt.html = f"{azimuth} {cx}:{cy} {str(destino)}"
                drone.segue(destino.partida(), azimuth, x, y, 0.3)

        # noinspection PyAttributeOutsideInit
        class Drone(J.a):
            """ Um drone que desvia para esquerda ou direita ao chocar com o anteparo

            As legendas aparecem inicialmente no local certo e depois de um intervalo vão para o canto esquerdo

            :param _: a posição horizontal do anteparo
            :param jogo: o jogo que este anteparo aparece
            :param cena_: a cena onde o anteparo aparece
            :param img: imagem de fundo do anteparo
            """

            def __init__(self, _, cena_, jogo, img=self.DRONE):
                pw = ph = Droner.KNOBS
                # print ("Drone.__init__", index, cena, jogo, img)
                self.jogo, self.cena = jogo, cena_
                self.destino, x, y = None, 0, 0  # self.jogo.localiza(index)
                # print ("Drone.__init__", self.index,x, y, azimuth)
                # x, y, _ = [(coor + GAP//4) if isinstance(int,coor) else coor for coor in self.jogo.localiza(index)]
                x, y = [(coor + GAP // 4) for coor in (x, y)]
                # print (x, y, _)
                super().__init__(img, x=x, y=y, w=pw, h=ph, style=SF, cena=cena_)
                # self.elt.style.transition = "left 1s top 1s"
                self.elt.ontransitionend = self.chegar
                self.rotate = 0
                self.azimuth = ROSA.oeste

            def chegar(self, _=None):
                """Quando o jogador acerta, apaga as interrogações da lacuna e posiciona a legenda sobre a lacuna"""
                self.destino.cheguei(self, self.azimuth)

            def segue(self, destino, azimuth, x, y, o):
                """Quando o drone bate na borda ele segue o azimuth sorteado"""
                self.destino, self.azimuth, self.o = destino, azimuth, o
                self.x, self.y = [(coor + GAP // 4) for coor in (x, y)]
                # self.elt.html = f"{str(self.destino)}#{self.azimuth}"
                # self.elt.html = f"{type(self.destino)}"

            def inicia(self, _=None, az=None):
                """Quando o jogador acerta, apaga as interrogações da lacuna e posiciona a legenda sobre a lacuna"""
                # self.entra(self.cena)
                # self.elt.style = SF
                self.o = 0.3
                # dx, dy = az or self.azimuth
                self.destino, x, y = self.jogo.localiza(22)  # , dx, dy)
                # self.chegar()
                self.segue(self.destino, self.azimuth, x, y, 0.3)
                # = self.index + dx + dy*11
                # self.x = self.x + dx*GAP*2
                # self.y = self.y + dy*GAP*2
                # self.x, self.y = [(coor + GAP // 4) for coor in (x, y)]
                # self.azimuth = az or self.azimuth
                # self.elt.html = f">{str(self.destino)}|{self.azimuth}"

        self.cena = cena
        self._a, self._b = Anteparo, Borda
        self.rotate = 0
        self.w = 11
        self.drone = Drone(0, cena, self)
        self.anteparos = [self.cria(index) for index in range(self.w * 6)]
        self.start()

    def start(self):
        self.drone.o = 0.3
        set_timeout(self.inicia, "1000")
        pass

    def cria(self, index):
        w, cena = self.w, self.cena
        x, y = (index % w), (index // w)
        good = 0 < x < 10 and 0 < y < 5
        return self._a(x, y, cena, self) if good else self._b(x, y, cena, self)

    def inicia(self, _=0):
        self.drone.inicia()

    def localiza(self, index, dx=0, dy=0):
        index = index + dx + self.w * dy
        # print("localiza", index)
        return self.anteparos[index].localiza()

    def rodar(self, rodado=0):
        """Quando o jogador acerta, apaga as interrogações da lacuna e posiciona a legenda sobre a lacuna"""
        self.rotate = rodado
        [anteparo.roda(rodado) for anteparo in self.anteparos]


def main(_=None):
    STYLE.update(width=1920, height="1080px")
    cena = J.c(Droner.CENA)
    Droner(cena)
    cena.vai()


if __name__ == "__main__":
    main()
