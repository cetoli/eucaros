#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Greeting card

    A Simple greeting card

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Classes neste módulo:

    :py:class:`Card` The greeting Card.

Changelog
---------
.. versionadded::    21.04
        Card creation.


.. seealso::

   Page :ref:`eucaros_introduction`

"""
from collections import namedtuple
J = B = namedtuple("lib", "a c html")


class Bicho:
    IMGUR = "https://i.imgur.com/{}.png"
    PANDA = "https://i.imgur.com/iVzWpXs.png"
    ANIMA = "https://i.imgur.com/JAxn218.png"
    FLOWER = namedtuple("Ticons", "fa fb fc fd")(
        *[IMGUR.format(im) for im in "8RqnodS YT8ROYG XIsrtJM UsCnCIw".split()])
    Icons = namedtuple("Ticons", "imagem x y")
    BICHO = namedtuple("Bicons", "panda animal fa fb fc fd")(
        Icons(PANDA, 4, 4), Icons(ANIMA, 7, 3),
        Icons(FLOWER.fa, 1, 1), Icons(FLOWER.fb, 1, 1), Icons(FLOWER.fc, 1, 1), Icons(FLOWER.fd, 1, 1))

    def __init__(self, cena, x=0, y=0, w=100, h=100, folha=None, icon=0):
        folha = folha or Bicho.BICHO.panda
        # style = {"color": f"#{c}", "font-size": f"{s}px", "font-family": font}
        self.banner = J.a(folha.imagem, x=x, y=y, w=w, h=h, cena=cena)
        size = (w*folha.x, h*folha.y)
        posicon = (-w*(icon % folha.x), -h*(icon//folha.x))
        self.banner.siz = size
        self.banner.pos = posicon


class Banner:
    VAZIO = "https://i.imgur.com/npb9Oej.png"
    FONT = 'runas beat blackriver blackroadster blendhes epicave potra pyra quirky' \
           ' rallisha hargrey baghotta beatting bratsyscript bratsyoutline' \
           ' hadfieldstrip quechely night rolling tradesmith wayawaya weslona' \
           ' rivandell roadstore boatman ethna nada neoneon okami'.split()

    def __init__(self, cena, texto, x=0, y=0, w=1000, h=600, s=180, c='FFFFEE', font=None, p=0):
        font = font or self.FONT[0]
        style = {"color": f"#{c}", "font-size": f"{s}px", "font-family": font}
        self.banner = J.a(self.VAZIO, x=x, y=y, w=w, h=h, cena=cena)
        self.banner.elt.style.padding = f"{p}px"
        self.texto = B.html.SPAN(texto, style=style)
        _ = self.banner.elt <= self.texto


class Card:
    CENA_ = 'https://images.unsplash.com/photo-1587930778465-0697740bd998?ixid=' \
            'MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1054&q=80'
    CENA = "https://www.thewowstyle.com/wp-content/uploads/2015/01/" \
           "1-beautiful-nature-backgrounds-nature-wallpapers-nature-landscape-photo-nature-wallpapers.jpeg"
    VAZIO = "https://i.imgur.com/npb9Oej.png"
    LOFLI = "https://loremflickr.com/1280/720"
    PLKIT = "https://placekitten.com/1280/720"
    LOPIX = "http://lorempixel.com/1280/720"
    PLIMG = "http://placeimg.com/1280/720/nature"

    def __init__(self):
        # self.cena = J.c(self.CENA)
        self.cena = J.c(self.PLIMG)
        self.cena.vai()

    def message(self):
        Banner(texto="BOA NOITE", p=30, s=160, y=40, w=1200, h=600, cena=self.cena, font="blendhes")
        Banner(texto="Para todo o nosso Clan!",
               p=30, s=160, x=250, y=280, w=1000, h=600, cena=self.cena, font="blendhes")
        kwargs = dict(cena=self.cena, w=100, h=100, folha=Bicho.BICHO.animal)
        [Bicho(icon=icon, y=480, x=105*icon, **kwargs) for icon in range(7)]
        [Bicho(icon=icon, y=580, x=105*(icon-7), **kwargs) for icon in range(7, 14)]
        [Bicho(icon=icon, y=680, x=105*(icon-14), **kwargs) for icon in range(14, 21)]
        Bicho(w=1000, h=700, cena=self.cena, folha=Bicho.BICHO.fd)

    def symb(self):
        Banner(texto=''.join(chr(ch) for ch in range(7000, 12000)),
               w=1000, h=800, s=48, cena=self.cena, font="quirksymbol")

    def menu(self):
        kwargs = dict(x=100, w=1000, h=600, s=48, cena=self.cena)
        bana, banb = Banner.FONT[:len(Banner.FONT)//2], Banner.FONT[len(Banner.FONT)//2:]
        [Banner(texto=f"Para o Clan {fon}", y=60+50*pos, font=fon, **kwargs) for pos, fon in enumerate(bana)]
        kwargs = dict(x=700, w=1000, h=600, s=48, cena=self.cena)
        [Banner(texto=f"Para o Clan {fon}", y=60+50*pos, font=fon, **kwargs) for pos, fon in enumerate(banb)]


def main(j, brython):
    """Inicia o jogo mostrando a primeira Cena.

    :param j: referência para a lib Vitollino.
    :param brython: referência para a lib Brython.
    :return: A instância de Jogo Eucaros e a cena corrente
    """
    global J, B
    J, B = j, brython
    Card().message()
    # Card().menu()
    # Card().symb()


if __name__ == "__main__":
    def offline_main():
        from unittest.mock import MagicMock

        class Browser:
            mock = MagicMock()
            document, alert, html, window, ajax, timer, svg = [mock] * 7

        main(Browser, Browser)


    # _init()
    # _routes()
    offline_main()
