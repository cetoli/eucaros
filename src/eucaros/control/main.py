#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Serve dynamic version information.

    Interface Multifuncional de Aquisição de Dados e Inteligência Sensorial..

.. codeauthor:: Carlo Oliveira <carlo@ufrj.br>

Classes neste módulo:

    :py:class:`Cena` Scenery controller.

    :py:class:`Dradis` Long range sensor controller.

    :py:class:`Eucaros` Main game constructor.

Changelog
---------
.. versionadded::    21.03
        Icon catalogue.

    21.02
        * NEW: Modules main and __version__.
        * NEW: Show first scene.
        * NEW; Dradis and Cena classes.

.. seealso::

   Page :ref:`eucaros_introduction`

"""
from eucaros.control.imadis import Imadis


class Cena(object):
    IMGUR = "https://i.imgur.com/{}.jpg"
    CENAS = [
        'q308L34', 'v1CVsjO', '7cXtyqA', 'jHnybc0', 'WyiiE0T', 'KZoXpRI',
        'iILubFM', 'regZ51L', 'fUDoUc0', 'CljPeMU', 'bDqM8oP', 'SghQp9q',
        'o8Hj2F4', 'ctDxOKA', 'gWvkAiR', 'SJ428kZ', 'kQWpngJ']

    def __init__(self, img=None, jogo=None, dradis=None):
        """ Uma cena em uma coordenada do jogo.

        :param img: índice da imagem na lista CENAS.
        :param jogo: referência para jogo na lib Vitollino.
        :param dradis: referência para Sensores e Atuadores Dradis.
        """
        self.j = jogo
        self.dradis = dradis
        self.img = img
        self.cena = self.gonial = self.urgo = self.clauson = self.warrior = None
        self.cena = self.j.c(self.IMGUR.format(self.CENAS[self.img]), nome=f"cena_{self.img}")

    def vai(self):
        """Ativa esta cena.

        :return: O elemento cena Vitollino
        """
        self.cena.vai()
        self.gonial = [Cena(img, self.j, self.dradis) for img in range(1, 5)]
        self.dradis.fit_dradis(self.cena, self.gonial)
        return self.cena


class Dradis(object):
    ICONS = 'far fa-eye fa-3x,fas fa-medal fa-4x,fas fa-shield-alt fa-5x'.split(",")
    ICONW = 'fas fa-user-ninja fa-2x,fas fa-user-shield fa-2x,fas fa-user-alt fa-2x,fas fa-user-secret fa-2x'.split(",")
    URGO = 'white:plus green:exclamation-triangle red:exclamation-circle purple:square blue:times'.split()
    GONIA = 'green:up red:right purple:left blue:down'.split()
    BLANK = "https://i.imgur.com/npb9Oej.png"

    def __init__(self, jogo, cena, brython=None):
        """

        :param jogo: referência para jogo na lib Vitollino.
        :param cena: Uma cena em uma coordenada do jogo.
        :param brython: referência para a lib Brython.
        """
        self.j = jogo
        self.cena_ = Cena(cena, jogo, self)
        """Cena corrente no Dradis"""
        self.cena = self.cena_.cena
        self.by = brython
        self.gonion = [self]*4
        """lista das cenas adjacentes"""
        self.horzal = self.patror = self.ergor = self.clauson = self
        self.unique = [self.patror, self.ergor, self.clauson]
        self.gonial = self.urgo = self.clavin = [self]*2
        self.quator = [self.gonial, self.urgo, self.clavin]
        """Lista dos atuadores com wuatro entradas"""
        self.elt = jogo.a("", tit="__dradis__").elt

    def entra(self, _):
        pass

    def build_dradis(self):
        """Constrói a interface Dradis."""
        return self.cena_

    def build_dradis_(self):
        """Constrói a interface Dradis.

        :return: A cena que atualmente mostra no Dradis.
        """
        self.patror = self.j.a(self.BLANK, w=60, h=50, x=600, y=600, cena=self.cena)
        _ = self.patror.elt <= self.by.html.V(Class=self.ICONS[0])
        self.ergor = self.j.a(self.BLANK, w=90, h=90, x=40, y=400, cena=self.cena)
        _ = self.ergor.elt <= self.by.html.V(Class=self.ICONS[1])
        self.clauson = self.j.a(self.BLANK, w=90, h=90, x=600, y=100, cena=self.cena)
        _ = self.clauson.elt <= self.by.html.V(Class=self.ICONS[2])
        self.clavin = [self.j.a(self.BLANK, w=90, h=90, x=300 + dx * 60, y=680, cena=self.cena)
                       for dx in range(4)]
        _ = [war.elt <= self.by.html.V(Class=icon) for war, icon in zip(self.clavin, self.ICONW)]
        self.urgo = [self.j.a(self.BLANK, w=90, h=90, x=int(900 + dx * 90), y=500 + dy * 90, cena=self.cena)
                     for dx, dy in [(1, 0), (2, 1), (0, 1), (1, 2)]]
        urgo = [self.by.html.SPAN(style=dict(color=icon.split(":")[0]))
                for icon in self.URGO[1:]]
        _ = [span <= self.by.html.V(Class="fas fa-3x fa-" + icon.split(":")[1])
             for span, icon in zip(urgo, self.URGO[1:])]
        _ = [act.elt <= span for act, span in zip(self.urgo, urgo)]
        self.gonial = [self.j.a(self.BLANK, w=90, h=90, x=int(200 + dx * 80), y=400 + dy * 80, cena=self.cena)
                       for dx, dy in [(1, 0), (2.2, 1), (0, 1), (1, 2)]]
        gonial = [self.by.html.SPAN(style=dict(color=icon.split(":")[0]))
                  for icon in self.GONIA]
        _ = [span <= self.by.html.V(Class="fas fa-5x fa-caret-" + icon.split(":")[1])
             for span, icon in zip(gonial, self.GONIA)]
        _ = [act.elt <= span for act, span in zip(self.gonial, gonial)]
        for cena_, icon in enumerate(self.gonial):
            icon.vai = lambda _=0, cen_=cena_: self.gonion[cen_].vai()
        self.unique = [self.patror, self.ergor, self.clauson]
        self.quator = [self.gonial, self.urgo, self.clavin]
        return self.cena_

    def vai(self):
        pass

    def fit_dradis(self, cena, gonion):
        """

        :param cena: A cena que atualmente mostra no Dradis.
        :param gonion: lista das cenas adjacentes
        :return: None
        """
        self.gonion = gonion
        # for cena_, icon in zip(gonion, self.gonial):
        #     icon.vai = cena_.vai
        for unique in self.unique:
            print(unique.elt.id, cena.img)
            unique.entra(cena)
        for quator in self.quator:
            for unique_ in quator:
                unique_.entra(cena)


class Eucaros(object):

    def __init__(self, brython):
        """Jogo de aventura em um mundo floresta, interior de um humano.

        :param brython:  referência para a lib Brython.
        """
        from _spy.vitollino.main import Jogo, STYLE
        STYLE.update(width=1280, height="720px")
        self.j = Jogo()
        self.by = brython
        # self.cena = self.j.c(self.IMGUR.format(self.CENAS[0]))
        self.dradis = Dradis(self.j, 0, self.by)
        self.setrum = self.cena = self.dradis.build_dradis().cena
        self.imadis = Imadis(cena=self.cena, v=self.j, b=brython)

    def vai(self):
        """Inicia o jogo mostrando a primeira Cena.

        :return: A cena que atualmente mostra no Dradis.
        """
        self.imadis.vai()
        # _ = self.j.c().elt <= self.by.document["__inv__"]
        return self.cena


def main(brython):
    """Inicia o jogo mostrando a primeira Cena.

    :param brython: referência para a lib Brython.
    :return: A instância de Jogo Eucaros e a cena corrente
    """
    def _render(*_):
        return eucaros.vai()

    eucaros = Eucaros(brython)

    brython.timer.set_timeout(_render, 1000)
    return eucaros, _render


if __name__ == "__main__":
    def offline_main():
        from unittest.mock import MagicMock

        class Browser:
            mock = MagicMock()
            document, alert, html, window, ajax, timer, svg = [mock] * 7

        epydemic, render = main(Browser)
        return epydemic, render


    # _init()
    # _routes()
    offline_main()
