#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Eucaros
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Connects and operates CRUD with Atlas database.

.. seealso::

   Page :ref:`eucaros_introduction`

"""
from pydantic import BaseModel
# from bson.objectid import ObjectId
from pymongo import MongoClient
# import motor.motor_asyncio
import os
passw = os.environ['EUCAROS_MALANTA']
print(passw)
# MONGO_DETAILS = "mongodb://localhost:27017"
MONGO_DETAILS = f"mongodb+srv://carlotolla:{passw}@eucaros.pothm.mongodb.net/malanta?retryWrites=true&w=majority"
MONGO_DETAILS += '&ssl=true&ssl_cert_reqs=CERT_NONE'
RDATA0 = """Cânion Itaimbezinho@cânion@29° 11' 30" S 50° 5' 51" O
Cachoeira das Andorinhas@cachoeira@29° 11' 30" S 50° 5' 51" O
Cascata Véu de Noiva@cascata@29° 11' 30" S 50° 5' 51" O
Rio do Boi@rio@29° 11' 30" S 50° 5' 51" O""".split("\n")
LRDATA0 = [relevo.split("@") for relevo in RDATA0]
DRDATA0 = [dict(nome=n, tipo=t, loc=l) for n, t, l in LRDATA0]
FAUNA0 = [{'nome': k.split("@")[0], 'espécie':k.split("@")[1]}
          for k in """papagaios-de-peito-roxo@(Amazona vinacea)
lobos-guará@(Chrysocyon brachyurus)
jaguatiricas@(Leopardus pardalis)
guaxinims@(Procyon lotor) 
leão-baio@(Felis concolor)""".split("\n")]
DATA0 = {k.split("@")[0]: k.split("@")[1] for k in """Nome@Aparados da Serra
Estados@Rio Grande do Sul Santa Catarina
Mesorregiões@Nordeste Rio-Grandense Sul Catarinense
Microrregiões@Vacaria Araranguá
Localidades mais próximas@Praia Grande, Cambará do Sul
Área@13 141,05 hectares (131,4 km2)[1]
Criação@17 de dezembro de 1959 (61 anos)
Visitantes@52 800 (em 2011)
Gestão@ICMBio
Sítio oficial@ICMBio - Parque Nacional de Aparados da Serra
Coordenadas@29° 11' 30" S 50° 5' 51" O""".split("\n")}
DATA0.update(Relevo=DRDATA0, Fauna=FAUNA0)
RELEVO = [{'nome': k.split("@")[0], 'tipo':k.split("@")[1], 'loc':k.split("@")[2]}
          for k in """Rio Chapecó@rio@26° 45' 53" S 51° 58' 3" O
Rio Chapecozinho@rio@26° 45' 53" S 51° 58' 3" O
Rio do Mato @rio@26° 45' 53" S 51° 58' 3" O
Lago Poço Verde@lago@26° 45' 53" S 51° 58' 3" O
Queda d’água Vale da Onça@queda@26° 45' 53" S 51° 58' 3" O
Cascata dos Morcegos,@cascata@26° 45' 53" S 51° 58' 3" O 
Queda d’água Barra do Vau@queda@26° 45' 53" S 51° 58' 3" O
Trilha Tapera das Flores@trilha@26° 45' 53" S 51° 58' 3" O
Trilha da Casa da Árvore@trilha@26° 45' 53" S 51° 58' 3" O
Trilha das Imbuias@trilha@26° 45' 53" S 51° 58' 3" O
Trilha do Morro Grande@trilha@26° 45' 53" S 51° 58' 3" O
Trilha da Campina@trilha@26° 45' 53" S 51° 58' 3" O""".split("\n")]
FAUNA = [{'nome': k.split("@")[0], 'espécie':k.split("@")[1]} for k in """papagaios-de-peito-roxo@(Amazona vinacea)
lobos-guará@(Chrysocyon brachyurus)
jaguatiricas@(Leopardus pardalis)
guaxinims@(Procyon lotor)
gralha azul@()
macuco@()
inambu@()
jacu@()
curicaca@()
surucuás@()
lontra@()
onça-parda@()
cachorro-do-mato@()
quati@()
veado@()
capivara@()
tatu@()
pica-pau-do-campo@()""".split("\n")]
DATA = {k.split("@")[0]: k.split("@")[1] for k in """Nome@Araucárias
Estado@Santa Catarina
Mesorregião@Oeste Catarinense
Microrregião@Xanxerê
Localidades mais próximas@Passos Maia e Ponte Serrada
Área@12 841 hectares (128,4 km2)
Criação@19 de outubro de 2005 (15 anos)
Visitantes@Não disponível
Gestão@ICMBio[1]
Coordenadas@26° 45' 53" S 51° 58' 3" O""".split("\n")}
DATA.update(Relevo=RELEVO, Fauna=FAUNA)


def parque_helper(student) -> dict:
    return {
        "id": str(student["_id"]),
        "cont": str(student),
        # "nome": student[0]["nome"]
    }


def main():
    def retrieve_students():
        _parques = []
        for student in parques.find():
            _parques.append(parque_helper(student))
        return _parques

    # client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
    client = MongoClient(MONGO_DETAILS)
    database = client.malanta
    print(client.list_database_names())
    # parques = database.get_collection("parques")
    parques = database.parques
    # parque = [parque_helper(parque_) for parque_ in parques]
    parque = retrieve_students()
    return parque


def include():
    def retrieve_students():
        _parques = []
        for student in parques.find():
            _parques.append(parque_helper(student))
        return _parques
    client = MongoClient(MONGO_DETAILS)
    database = client.malanta
    print(client.list_database_names())
    # parques = database.get_collection("parques")
    parques = database.parques
    # parque = [parque_helper(parque_) for parque_ in parques]
    parques.insert_one(DATA)
    parque = retrieve_students()
    return parque


def replace(data):
    def retrieve_students():
        _parques = []
        for student in parques.find():
            _parques.append(parque_helper(student))
        return _parques
    client = MongoClient(MONGO_DETAILS)
    database = client.malanta
    print(client.list_database_names())
    # parques = database.get_collection("parques")
    parques = database.parques
    # parque = [parque_helper(parque_) for parque_ in parques]
    parques.replace_one({"Coordenadas": data["Coordenadas"]}, data)
    parque = retrieve_students()
    return parque

# helpers


def student_helper(student) -> dict:
    return {
        "id": str(student["_id"]),
        "fullname": student["fullname"],
        "email": student["email"],
        "course_of_study": student["course_of_study"],
        "year": student["year"],
        "GPA": student["gpa"],
    }


class Item(BaseModel):
    _id: str  # 1
    is_available: bool


def test_pydantic_hides_names_with_preceding_underscores():
    item = Item(_id='test-item-id', is_available=True)  # 1
    assert hasattr(item, '_id') is False  # 2
    assert item.dict().get('_id') is None  # 2


if __name__ == '__main__':
    [print({k: v}) for k, v in DATA.items()]
    [print(dat) for dat in replace(DATA)]
    [print(dat) for dat in replace(DATA0)]
    # print(main())
    # print(replace(DATA))
    # print(replace(DATA0))
